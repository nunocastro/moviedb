package com.nunocastro.moviedb.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nunocastro.moviedb.R;
import com.nunocastro.moviedb.objects.MovieObject;
import com.nunocastro.moviedb.utils.MovieDBUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ncastro on 2/16/16.
 */
public class MovieListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEWTYPE_ITEM = 1;
    public static final int VIEWTYPE_LOADER = 2;

    private ArrayList<MovieObject> dataset;
    private boolean showFooter = false;

    private static class MovieObjectHolder extends RecyclerView.ViewHolder {
        ImageView poster;
        TextView name;
        TextView category;
        TextView year;
        TextView popularity;

        public MovieObjectHolder(final View itemView) {
            super(itemView);
            poster = (ImageView) itemView.findViewById(R.id.poster);
            name = (TextView) itemView.findViewById(R.id.name);
            category = (TextView) itemView.findViewById(R.id.category);
            year = (TextView) itemView.findViewById(R.id.year);
            popularity = (TextView) itemView.findViewById(R.id.popularity);
        }
    }

    private static class ProgressHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        public ProgressHolder(final View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }

    public MovieListAdapter(final ArrayList<MovieObject> data) {
        this.dataset = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;

        switch (viewType) {
            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_loader, parent, false);
                viewHolder = new ProgressHolder(view);
                break;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_card, parent, false);
                viewHolder = new MovieObjectHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ProgressHolder) {
            ProgressHolder progressHolder = (ProgressHolder) holder;
            progressHolder.progressBar.setVisibility(showFooter ? View.VISIBLE : View.GONE);

        } else {
            MovieObjectHolder movieHolder = (MovieObjectHolder) holder;
            MovieObject movieObject = dataset.get(position);

            movieHolder.name.setText(movieObject.getTitle());
            movieHolder.category.setText(movieObject.getMainGenre());
            movieHolder.year.setText(movieObject.getReleaseYear());
            movieHolder.popularity.setText(movieObject.getPopularityFormated());

            Picasso.with(movieHolder.poster.getContext())
                    .load(MovieDBUtils.getCompleteImageURL(MovieDBUtils.POSTER_CARD_SIZE, movieObject.getPosterPath()))
                    .into(movieHolder.poster);
        }
    }

    @Override
    public int getItemCount() {
        // if we have items, add one more row for the loader at the footer
        return null == this.dataset || this.dataset.isEmpty() ? 0 : this.dataset.size() + 1;
    }

    @Override
    public int getItemViewType(final int position) {
        // loader can only be at the last position
        return position != 0 && position == getItemCount() - 1 ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
    }

    public void setShowLoading(final boolean value) {
        showFooter = value;
    }
}
