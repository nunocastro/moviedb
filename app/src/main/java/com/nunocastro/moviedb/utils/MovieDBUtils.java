package com.nunocastro.moviedb.utils;

import com.nunocastro.moviedb.objects.MovieGenresObject;
import com.nunocastro.moviedb.requests.RESTGenerator;

import java.util.ArrayList;

/**
 * Created by ncastro on 2/16/16.
 */
public class MovieDBUtils {

    public static final String POSTER_CARD_SIZE = "w342";
    public static final String POSTER_DETAIL_SIZE = "w500";

    private static ArrayList<MovieGenresObject> genres = new ArrayList<>();

    public static String getCompleteImageURL(String size, String imagePath) {
        return RESTGenerator.MOVIEDB_IMAGE_BASE_URL + size + imagePath;
    }

    public static void setGenres(ArrayList<MovieGenresObject> newGenres) {
        genres.addAll(newGenres);
    }

    public static String getGenderName(final int id) {
        String genderName = "";
        MovieGenresObject genderObject;
        for (int i = 0; i < genres.size() && genderName.isEmpty(); i++) {
            genderObject = genres.get(i);
            if (id == genderObject.getId()) {
                genderName = genderObject.getName();
            }
        }

        return genderName;
    }
}
