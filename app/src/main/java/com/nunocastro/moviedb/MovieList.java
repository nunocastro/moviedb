package com.nunocastro.moviedb;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.nunocastro.moviedb.adapters.MovieListAdapter;
import com.nunocastro.moviedb.objects.MovieGenresObject;
import com.nunocastro.moviedb.objects.MovieListObject;
import com.nunocastro.moviedb.objects.MovieObject;
import com.nunocastro.moviedb.requests.MovieGenresRequest;
import com.nunocastro.moviedb.requests.MovieRequest;
import com.nunocastro.moviedb.utils.MovieDBUtils;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieList extends AppCompatActivity {

    private static final String GENRES = "genres";
    private static final int GRID_NUM_COLUMNS = 2;
    private static final int BOTTOM_ITEM_LIMIT = 6;
    private static final int NO_MORE_PAGES = -1;

    private ArrayList<MovieObject> movies;
    private RecyclerView moviesRecyclerView;
    private SwipeRefreshLayout refreshContainer;
    private FloatingActionButton backToTopButton;

    private boolean isExecutingRequest = false;
    private boolean isRefreshing = false;
    private int nextPage = 1;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        movies = new ArrayList<>();

        new MovieGenresRequest()
                .setResponseCallback(movieGenderCallback)
                .execute();

        refreshContainer = (SwipeRefreshLayout) findViewById(R.id.refresh_container);
        refreshContainer.setOnRefreshListener(onRefreshListener);

        GridLayoutManager gridManager = new GridLayoutManager(this, GRID_NUM_COLUMNS);
        gridManager.setSpanSizeLookup(spanSizeLookup);

        moviesRecyclerView = (RecyclerView) findViewById(R.id.movies_list);
        moviesRecyclerView.setHasFixedSize(true);
        moviesRecyclerView.setLayoutManager(gridManager);
        moviesRecyclerView.addOnScrollListener(onScrollListener);

        backToTopButton = (FloatingActionButton) findViewById(R.id.back_top_fab);
        backToTopButton.setOnClickListener(onClickListener);

        this.setTitle(String.format(getString(R.string.app_title), getString(R.string.movies_sort_popular)));
    }


    private Callback<HashMap<String, ArrayList<MovieGenresObject>>> movieGenderCallback = new Callback<HashMap<String, ArrayList<MovieGenresObject>>>() {
        @Override
        public void onResponse(final Call<HashMap<String, ArrayList<MovieGenresObject>>> call, final Response<HashMap<String, ArrayList<MovieGenresObject>>> response) {
            MovieDBUtils.setGenres(response.body().get(GENRES));

            requestMovieListPage(nextPage);
        }

        @Override
        public void onFailure(final Call<HashMap<String, ArrayList<MovieGenresObject>>> call, final Throwable t) {
            t.printStackTrace();
        }
    };

    private Callback<MovieListObject> movieListCallback = new Callback<MovieListObject>() {
        @Override
        public void onResponse(final Call<MovieListObject> call, final Response<MovieListObject> response) {
            Log.d("_onResponse", "SUCCESS");

            if (response.isSuccess()) {
                if (nextPage == 1) {
                    movies.clear();
                }
                movies.addAll(updateMainGender(response.body().getResults()));

                MovieListAdapter movieListAdapter = (MovieListAdapter) moviesRecyclerView.getAdapter();
                if (null == movieListAdapter) {
                    movieListAdapter = new MovieListAdapter(movies);
                    moviesRecyclerView.setAdapter(movieListAdapter);
                } else {
                    movieListAdapter.setShowLoading(false);
                    movieListAdapter.notifyDataSetChanged();
                }

                if (response.body().getPage() != response.body().getTotalPages()) {
                    nextPage++;
                } else {
                    nextPage = NO_MORE_PAGES;
                }
                isExecutingRequest = false;
                if (isRefreshing) {
                    refreshContainer.setRefreshing(false);
                    isRefreshing = false;
                }

            } else {
                Log.d("_onResponse", "Not success: " + response.message());
            }

        }

        @Override
        public void onFailure(final Call<MovieListObject> call, final Throwable t) {
            t.printStackTrace();
        }
    };

    private final RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (((GridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition() > 2) {
                backToTopButton.setVisibility(View.VISIBLE);
            } else {
                backToTopButton.setVisibility(View.GONE);
            }

            if (((GridLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition() > recyclerView.getLayoutManager().getItemCount() - BOTTOM_ITEM_LIMIT) {
                MovieListAdapter movieListAdapter = (MovieListAdapter) moviesRecyclerView.getAdapter();
                movieListAdapter.setShowLoading(true);
                movieListAdapter.notifyDataSetChanged();

                requestMovieListPage(nextPage);
            }
        }
    };

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            nextPage = 1;
            isRefreshing = true;
            requestMovieListPage(nextPage);
        }
    };

    private GridLayoutManager.SpanSizeLookup spanSizeLookup = new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
            int spanCount;
            MovieListAdapter movieListAdapter = (MovieListAdapter) moviesRecyclerView.getAdapter();
            switch (movieListAdapter.getItemViewType(position)) {
                case MovieListAdapter.VIEWTYPE_LOADER:
                    spanCount = GRID_NUM_COLUMNS;
                    break;

                default:
                    spanCount = 1; // will display on it's own space
                    break;
            }
            return spanCount;
        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            moviesRecyclerView.scrollToPosition(10);
            moviesRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    moviesRecyclerView.smoothScrollToPosition(0);
                }
            });
        }
    };

    private void requestMovieListPage(int page) {
        if (NO_MORE_PAGES != page && !isExecutingRequest) {
            isExecutingRequest = true;
            new MovieRequest()
                    .setSort(MovieRequest.SORT_POPULARITY)
                    .setResponseCallback(movieListCallback)
                    .setPage(page)
                    .execute();
        }
    }

    private ArrayList<MovieObject> updateMainGender(final ArrayList<MovieObject> movies) {
        final int FIRST_GENDER = 0;

        for (MovieObject movie : movies) {
            if (!movie.getGenreIds().isEmpty()) {
                movie.setMainGenre(movie.getGenreIds().get(FIRST_GENDER));
            }
        }

        return movies;
    }

}
