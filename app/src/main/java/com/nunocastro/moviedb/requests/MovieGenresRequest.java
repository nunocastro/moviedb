package com.nunocastro.moviedb.requests;

import com.nunocastro.moviedb.objects.MovieGenresObject;
import com.nunocastro.moviedb.utils.MovieDBSettings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ncastro on 2/16/16.
 */
public class MovieGenresRequest {

    private Callback<HashMap<String, ArrayList<MovieGenresObject>>> responseCallback;
    private HashMap<String, ArrayList<MovieGenresObject>> response;


    public interface RequestMovieGenders {
        @GET("/3/genre/movie/list")
        Call<HashMap<String, ArrayList<MovieGenresObject>>> getMovieGenders(@Query("api_key") String apikey);
    }

    public MovieGenresRequest setResponseCallback(Callback<HashMap<String, ArrayList<MovieGenresObject>>> responseCallback) {
        this.responseCallback = responseCallback;
        return this;
    }

    public HashMap<String, ArrayList<MovieGenresObject>> getResponse() {
        return response;
    }

    public void execute() {
        RequestMovieGenders requestMovieGenders = RESTGenerator.createRequest(RequestMovieGenders.class);
        Call<HashMap<String, ArrayList<MovieGenresObject>>> call = requestMovieGenders.getMovieGenders(MovieDBSettings.API_KEY);

        if (null != responseCallback) {
            call.enqueue(responseCallback);
        } else {
            try {
                response = call.execute().body();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
    }

}
