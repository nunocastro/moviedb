package com.nunocastro.moviedb.requests;

import com.nunocastro.moviedb.objects.MovieListObject;
import com.nunocastro.moviedb.utils.MovieDBSettings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by ncastro on 2/16/16.
 */
public class MovieRequest {

    public static final String SORT_POPULARITY = "popular";
    public static final String SORT_LATEST = "latest";
    public static final String SORT_NOW_PLAYING = "now_playing";
    public static final String SORT_TOP_RATED = "top_rated";
    public static final String SORT_UPCOMING = "upcoming";

    private String sort = SORT_POPULARITY;
    private int page = 1;

    private Callback<MovieListObject> responseCallback;
    private MovieListObject response;

    public interface RequestMovieList {
        @GET("/3/movie/{sorting}")
        Call<MovieListObject> getMovieList(@Path("sorting") String sorting, @QueryMap Map<String, String> query);
    }

    public MovieRequest setSort(String sort) {
        this.sort = sort;
        return this;
    }

    public MovieRequest setResponseCallback(Callback<MovieListObject> responseCallback) {
        this.responseCallback = responseCallback;
        return this;
    }

    public MovieRequest setPage(int page) {
        this.page = page;
        return this;
    }

    public MovieListObject getResponse() {
        return response;
    }

    public void execute() {
        RequestMovieList requestMovieList = RESTGenerator.createRequest(RequestMovieList.class);
        Call<MovieListObject> call = requestMovieList.getMovieList(sort, getQueryParameters());

        if (null != responseCallback) {
            call.enqueue(responseCallback);
        } else {
            try {
                response = call.execute().body();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
    }

    private HashMap<String, String> getQueryParameters() {
        HashMap<String, String> queryParameters = new HashMap<>();

        queryParameters.put("api_key", MovieDBSettings.API_KEY);
        if (page > 1) {
            queryParameters.put("page", String.valueOf(page));
        }

        return queryParameters;
    }
}
