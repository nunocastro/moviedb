package com.nunocastro.moviedb.requests;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ncastro on 2/16/16.
 */
public class RESTGenerator {

    public static final String MOVIEDB_BASE_URL = "https://api.themoviedb.org/";
    public static final String MOVIEDB_IMAGE_BASE_URL = "http://image.tmdb.org/t/p/";

    private static final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    private static final OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();


    private static final Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(MOVIEDB_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <T> T createRequest(final Class<T> serviceClass) {
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final Retrofit retrofit = builder.client(
                okHttpClient
                        .addInterceptor(logging)
                        .build()
        ).build();
        return retrofit.create(serviceClass);
    }
}
