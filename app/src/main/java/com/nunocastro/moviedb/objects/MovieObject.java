package com.nunocastro.moviedb.objects;

import com.nunocastro.moviedb.utils.MovieDBUtils;

import java.util.ArrayList;

/**
 * Created by ncastro on 2/16/16.
 */
public class MovieObject {

    private String poster_path;
    private boolean adult;
    private String overview;
    private String release_date;
    private ArrayList<Integer> genre_ids;
    private long id;
    private String original_title;
    private String original_language;
    private String title;
    private String backdrop_path;
    private double popularity;
    private int vote_count;
    private boolean video;
    private double vote_average;

    private transient String mainGenre = "";

    public String getPosterPath() {
        return poster_path;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return release_date;
    }

    public String getReleaseYear() {
        return release_date.substring(0, 4);
    }

    public ArrayList<Integer> getGenreIds() {
        return genre_ids;
    }

    public long getId() {
        return id;
    }

    public String getOriginalTitle() {
        return original_title;
    }

    public String getOriginalLanguage() {
        return original_language;
    }

    public String getTitle() {
        return title;
    }

    public String getBackdropPath() {
        return backdrop_path;
    }

    public double getPopularity() {
        return popularity;
    }

    public String getPopularityFormated() {
        return String.format("%.2f", popularity);
    }

    public int getVoteCount() {
        return vote_count;
    }

    public boolean hasVideo() {
        return video;
    }

    public double getVoteAverage() {
        return vote_average;
    }

    public String getMainGenre() {
        return mainGenre;
    }

    public void setMainGenre(int genreId) {
        mainGenre = MovieDBUtils.getGenderName(genreId);
    }
}
