package com.nunocastro.moviedb.objects;

/**
 * Created by ncastro on 2/16/16.
 */
public class MovieGenresObject {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
