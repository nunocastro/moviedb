package com.nunocastro.moviedb.objects;

import java.util.ArrayList;

/**
 * Created by ncastro on 2/16/16.
 */
public class MovieListObject {

    int page;
    ArrayList<MovieObject> results;
    int total_results;
    int total_pages;

    public int getPage() {
        return page;
    }

    public ArrayList<MovieObject> getResults() {
        return results;
    }

    public int getTotalResults() {
        return total_results;
    }

    public int getTotalPages() {
        return total_pages;
    }

    public boolean isLastPage() {
        return page == total_pages;
    }
}
